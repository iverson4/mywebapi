using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Polly;
using Polly.Extensions.Http;
using System.Net;
using System.Net.Http.Headers;



namespace Winform;



static class Program
{
    private static IConfiguration _configuration;
    /// <summary>
    ///  The main entry point for the application.
    /// </summary>
    [STAThread]
    static void Main()
    {
        // To customize application configuration such as set high DPI settings or default font,
        // see https://aka.ms/applicationconfiguration.
        //ApplicationConfiguration.Initialize();
        //Application.Run(new Form1());
        var builder = new ConfigurationBuilder().AddJsonFile(path: "appsettings.json", optional: true, reloadOnChange: true);
        _configuration = builder.Build();
        var services = new ServiceCollection();
        ConfigureServices(services);
        using (ServiceProvider serviceProvider = services.BuildServiceProvider())
        {
            var form1 = serviceProvider.GetRequiredService<Form1>();
            //var form11 = serviceProvider.GetService<Form1>();
            Application.Run(form1);
        }
    }



    private static void ConfigureServices(ServiceCollection services)
    {
        var apiConfiguration = new ApiConfiguration();
        _configuration.Bind("Api", apiConfiguration);
        services.AddSingleton(apiConfiguration);



        services.AddSingleton<IConfiguration>(_ => _configuration);
        services.AddLogging(configure => configure.AddConsole());
        services.AddDbContext<ApplicationDbContext>(optionsAction: options =>
            options.UseSqlite(_configuration.GetConnectionString("DefaultConnection")));
        services.AddScoped<Form1>();
        services.AddScoped<Form2>();



        services.AddHttpClient("MyClient", client =>
        {
            client.BaseAddress = new Uri(apiConfiguration.Endpoint);
        }).ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler()
        {
            ClientCertificateOptions = ClientCertificateOption.Manual,
            ServerCertificateCustomValidationCallback = ((message, certificate2, arg3, arg4) => true)
        }).SetHandlerLifetime(TimeSpan.FromMinutes(20));



        services.AddHttpClient("MyAuthenticatedClient", client =>
        {
            client.BaseAddress = new Uri(apiConfiguration.Endpoint);
            client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apiConfiguration.AccessToken);
        })
            .ConfigurePrimaryHttpMessageHandler(() => new HttpClientHandler()
        {
            ClientCertificateOptions = ClientCertificateOption.Manual,
            ServerCertificateCustomValidationCallback =
            ((message, certificate2, arg3, arg4) => true)
            }).SetHandlerLifetime(TimeSpan.FromMinutes(20))
            .AddPolicyHandler(GetRetryPolicy());
       
    }

    private static IAsyncPolicy<HttpResponseMessage> GetRetryPolicy()
    {
        return HttpPolicyExtensions.
            HandleTransientHttpError().
            OrResult(msg =>
            msg.StatusCode == HttpStatusCode.Unauthorized ||
            msg.StatusCode == HttpStatusCode.NotFound ||
            msg.StatusCode == HttpStatusCode.InternalServerError ||
            msg.StatusCode == HttpStatusCode.RequestTimeout).WaitAndRetryAsync(6, retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt + 1)));
    }
}