﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Winform
{
    public class ApiConfiguration
    {
        public string Endpoint { get; set; }
        public string AccessToken { get; set; }
    }
}
