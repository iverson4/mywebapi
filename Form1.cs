using System.Text;
using System.Text.Json;



namespace Winform;



public partial class Form1 : Form
{
    private readonly Form2 _form2;
    private readonly IHttpClientFactory _httpClientFactory;
    public Form1(ApiConfiguration apiConfiguration,
        Form2 form2,
        IHttpClientFactory httpClientFactory)
    {
        _form2 = form2;
        _httpClientFactory = httpClientFactory;
        InitializeComponent();
    }



    private void Form1_Load(object sender, EventArgs e)
    {

    }



    private void button1_Click(object sender, EventArgs e)
    {
        _form2.ShowDialog();
    }



    private async void button2_Click(object sender, EventArgs e)
    {
        var client = _httpClientFactory.CreateClient("MyClient");
        var response = await client.GetAsync("posts");
        if (response.IsSuccessStatusCode)
        {
            var text = await response.Content.ReadAsStringAsync();
            var posts = JsonSerializer.Deserialize<List<Post>>(text, new JsonSerializerOptions()
            {
                PropertyNameCaseInsensitive = true
            });
            var indentedText = JsonSerializer.Serialize<List<Post>>(posts, new JsonSerializerOptions()
            {
                WriteIndented = true
            });
            textBox1.Text = indentedText;
        }
    }

    private async void button3_Click(object sender, EventArgs e)
    {
        var client = _httpClientFactory.CreateClient("MyAuthenticatedClient");
        var newPost = new Post()
        {
            UserId = 2616,
            Body = "Hello World",
            Title = "Hello Test"
        };
        var json = JsonSerializer.Serialize(newPost);
        var content = new StringContent(json, Encoding.UTF8, "application/json");
        var response = await client.PostAsync("posts", content);
        response.EnsureSuccessStatusCode();
        var status = await response.Content.ReadAsStringAsync();
        var createObject = JsonSerializer.Deserialize<Post>(status);
        if(createObject != null)
        {
            textBox1.Text = "Created at ID :" + createObject.Id;
        }
    }

    private async void button4_Click(object sender, EventArgs e)
    {
        var count = 1;
        restart:
        try
        {
            var client = _httpClientFactory.CreateClient("MyAuthenticatedClient");
            var response = await client.GetAsync("https://explorer.thaismartcontract.com/api/v2/status");
            response.EnsureSuccessStatusCode();
        }
        catch (Exception)
        {
            Thread.Sleep(count * 1000);
            count++;
            textBox1.Text += $"{DateTime.Now} : Error!{Environment.NewLine}";
            goto restart;
        }
    }

    private async void button5_Click(object sender, EventArgs e)
    {
        try
        {
            var client = _httpClientFactory.CreateClient("MyAuthenticatedClient");
            var response = await client.GetAsync("https://explorer.thaismartcontract.com/supapak");
            response.EnsureSuccessStatusCode();
        }
        catch(Exception)
        {
            MessageBox.Show("Finish");
        }
        
    }
}